import WS from 'jest-websocket-mock';
const chat = require('../dist/chat.umd');
const Server = require('mock-http-server');

const WS_URL = "ws://localhost:1234";
const URL = 'http://localhost:9001';
let ws;
let mockServer = new Server({
  host: 'localhost',
  port: 9001,
});;

describe('chat', () => {
  it('configura o chat', () => {
    const testChat = {
      HTTP_URL: 'url teste',
      WS_URL: 'ws teste',
      HEADERS: 'auth teste',
      USER: 1,
      CONNECTION: 'DISCONNECTED',
      onconnection: null,
    }
    chat.setConfig({
      HTTP_URL: 'url teste',
      WS_URL: 'ws teste',
      USER: 1,
      HEADERS: 'auth teste',
    });
    expect(chat.getConfig()).toEqual(testChat);
  });

  it('atualiza configuracao', () => {
    const testChat = {
      HTTP_URL: 'url teste',
      WS_URL: 'ws teste',
      HEADERS: 'auth teste',
      USER: 1,
      CONNECTION: 'DISCONNECTED',
    }
    const testChat2 = {
      HTTP_URL: 'url teste2',
      WS_URL: 'ws teste2',
      HEADERS: 'auth teste2',
      USER: 2,
      CONNECTION: 'DISCONNECTED',
      onconnection: null,
    }
    chat.setConfig(testChat);
    chat.setConfig(testChat2);
    expect(chat.getConfig()).toEqual(testChat2);
  });
});


describe('conexão websocket', () => {
  beforeEach(() => {
    ws = new WS(WS_URL);
    chat.setConfig({
      HTTP_URL: URL,
      WS_URL: WS_URL,
    })
    chat.connect();
  });

  afterEach(() => {
    WS.clean();
  });

  it('conecta o websocket', () => {
    expect(chat.getConfig().CONNECTION === 'CONNECTED');
  });

  it('desconecta o websocket', async () => {
    await chat.disconnect();
    expect(chat.getConfig()).CONNECTION === 'DISCONNECTED';
  })

  it('recebe mensagem', () => {
    let mensagem;
    let conversa;

    function updateMensagem(_mensagem, _conversa) {
      mensagem = _mensagem;
      conversa = _conversa;
    }
    chat.on('message', updateMensagem);
    ws.send(JSON.stringify({
      type: 'MESSAGE',
      message: 'teste',
      conversa: {
        id: 1
      },
    }));

    expect(mensagem).toEqual('teste');
    expect(conversa).toEqual({
      id: 1
    });
  })

  it('recebe novo status', () => {
    let pessoa1 = {
      id: 1,
      status: 'offline'
    };
    let pessoa2 = {
      id: 2,
      status: 'offline'
    };
    const pessoas = [pessoa1, pessoa2];

    function updateStatus(_status, _pessoa, _contatos) {
      pessoas.find(p => p.id === _pessoa.id).status = _status;
    }
    chat.on('status', updateStatus);
    ws.send(JSON.stringify({
      status: 'online',
      contatos: [1, 2],
      user: {
        id: 1
      },
      type: 'STATUS'
    }));

    expect(pessoa1.status).toEqual('online');
    expect(pessoa2.status).toEqual('offline');
  })

  it('recebe pessoa digitando', () => {
    let pessoa1 = {
      id: 1,
    }
    let pessoa2 = {
      id: 2,
    }
    let conversa = {
      id: 1,
      digitando: [pessoa1]
    }
    chat.on('typing', (_pessoa, _conversa) => {
      conversa = {
        ..._conversa,
        digitando: [...conversa.digitando, _pessoa]
      };

    })
    ws.send(JSON.stringify({
      type: 'TYPING',
      user: {
        id: 2
      },
      conversa: {
        id: 1
      },
      typing: true,
    }))
    expect(conversa).toEqual({
      id: 1,
      digitando: [pessoa1, pessoa2]
    });
  })

})

describe('conexão http', () => {
  beforeEach((done) => {
    chat.setConfig({
      HTTP_URL: URL,
      WS_URL: WS_URL,
      USER: 1,
      HEADERS: {
        "content-type": "application/json"
      },
    });
    mockServer.start(done);
  });

  afterEach((done) => {
    mockServer.resetHandlers();
    mockServer.stop(done);
  });

  it('recebe os contatos', async () => {
    mockServer.on({
      method: 'GET',
      path: '/chat/contatos',
      reply: {
        status: 200,
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({
          action: "leu contatos"
        })
      }
    });
    const response = await chat.getContatos();
    expect(response).toEqual({
      action: "leu contatos"
    });
  })
  it('recebe as conversas', async () => {
    mockServer.on({
      method: 'GET',
      path: '/chat/conversas',
      reply: {
        status: 200,
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({
          action: "leu conversas"
        })
      }
    });
    const response = await chat.getConversas();
    expect(response).toEqual({
      action: "leu conversas"
    });
  })
  it('adiciona contato', async () => {
    mockServer.on({
      method: 'POST',
      path: '/chat/contatos',
      reply: {
        status: 200,
        headers: {
          "content-type": "application/json"
        },
        body: (req) => {
          return JSON.stringify({
            action: "adicionou contato",
            contato: req.body
          })

        }
      }
    });
    const response = await chat.addContato({
      id: 1
    });
    expect(response).toEqual({
      action: "adicionou contato",
      contato: {
        id: 1
      }
    });
  })
  it('adiciona conversa', async () => {
    mockServer.on({
      method: 'POST',
      path: '/chat/conversas',
      reply: {
        status: 200,
        headers: {
          "content-type": "application/json"
        },
        body: (req) => {
          return JSON.stringify({
            action: "adicionou conversa",
            conversa: req.body
          })
        }
      }
    });
    const response = await chat.addConversa({
      id: 1
    });
    expect(response).toEqual({
      action: "adicionou conversa",
      conversa: {
        id: 1
      }
    });
  })
})
