'use strict';

const axios = require('axios');

let $websocket;
let $config = {
  HEADERS: null,
  HTTP_URL: null,
  WS_URL: null,
  USER: null,
  CONNECTION: 'DISCONNECTED',
  onconnection: null,
}
let $events = {}; // lista de eventos
let $axios;
let $storage = {};

/**
 * adiciona um listener de evento
 * @param {string} event evento para o listener
 * @param {Function} listener listener para receber os parametros do evento
 */
export function on(event, listener) { // adiciona listenners de eventos
  if (!$events[event]) {
    $events[event] = {
      listeners: []
    };
  }
  $events[event].listeners.push(listener);
}

/**
 * deleta um listener de evento
 * @param {string} event indicador do evento a ser deletado
 */
export function off(event) { // remove listeners
  delete $events[event];
}

/**
 * Aciona um listener de evento
 * @param {string} name Nome do evento a ser emitido
 * @param  {...any} payload Payload do evento
 */
function emit(name, ...payload) {
  if ($events[name]) {
    for (const listener of $events[name].listeners) {
      listener.apply(this, payload);
    }
  }
}

/**
 * Atualiza o storage
 */
function updateStorage() {
  window.localStorage.setItem('chatStorage', JSON.stringify($storage));
}

/**
 * @returns {object} retorna o objeto salvo no storage
 */
function getStorage() {
  const storage = window.localStorage.getItem('chatStorage');
  return JSON.parse(storage);
}

/**
 * Adiciona as configurações do chat para conexão com o server
 * @param {object} config objeto de configuração
 * @param {string} config.HTTP_URL url padrão para as requisições
 * @param {string} config.WS_URL url padrão para conexão com o WebSocket
 * @param {string} config.HEADERS header para tentar a conexão
 * @param {object} config.USER usuário que logou no chat
 * @param {function} config.onconnection listener de conexão @param {string} CONNECTION quando fica online recebe "CONNECTED" e offline "DISCONNECTED"
 */
export function setConfig(config) {
  $config = {
    ...$config,
    ...config
  };
  const headers = typeof $config.HEADERS === 'string' ? [$config.HEADERS] : $config.HEADERS;
  $axios = axios.create({
    baseURL: $config.HTTP_URL,
    headers,
  });
  window.ononline = () => {
    $config.CONNECTION = "CONNECTED";
    if ($config.onconnection) {
      $config.onconnection($config.CONNECTION);
    }
  }
  window.offline = () => {
    $config.CONNECTION = "DISCONNECTED";
    if ($config.onconnection) {
      $config.onconnection($config.CONNECTION);
    }
  }
}

/**
 * abre conexão com o websocket
 */
export function connect() {
  $websocket = new WebSocket($config.WS_URL);
  $websocket.onopen = () => {
    $config.CONNECTION = 'CONNECTED';
    if ($config.onconnection) {
      $config.onconnection($config.CONNECTION);
    }
    $websocket.send(JSON.stringify({
      status: $config.CONNECTION,
      user: $config.USER,
    }))
    $storage['user'] = $config.USER;
    updateStorage();
  }
  $websocket.onclose = () => {
    $config.CONNECTION = 'DISCONNECTED';
    if ($config.onconnection) {
      $config.onconnection($config.CONNECTION);
    }
  }
  $websocket.onmessage = (event) => {
    const data = JSON.parse(event.data);
    switch (data.status) {
      case 'MESSAGE': {
        if ($events.message && data) {
          if (data.file) {
            emit('file', data);
          } else {
            emit('message', data);
          }
        }
        break;
      }
      case 'STATUS': {
        if ($events.status && data.status && data.user) {
          emit('status', data);
        }
        break;
      }
      case 'TYPING': {
        if ($events.typing && data.typing && data.user) {
          emit('typing', data);
        }
        break;
      }
      case 'DISCONNECT': {
        $websocket.close();
        break;
      }
    }
  }
}

/**
 * Método para enviar mensagem
 * @param {string} message string da mensagem
 * @param {object} user usuário que enviou a mensagem
 * @param {object} conversa conversa de destino da mensagem
 */
export function sendMessage(message, user, conversa) {
  if ($websocket instanceof WebSocket && $config.CONNECTION === 'CONNECTED') {
    const _message = {
      user,
      message,
      conversa,
      timestamp: new Date().getTime(),
      status: 'MESSAGE'
    }
    $websocket.send(JSON.stringify(_message));
  }
}

/**
 * Método para enviar arquivos
 * @param {File} file arquivo a ser enviado
 * @param {object} user usuário que enviou a mensagem
 * @param {object} conversa conversa de destino da mensagem
 */
export function sendFile(file, user, conversa) {
  if ($websocket instanceof WebSocket && $config.CONNECTION === 'CONNECTED') {
    const mimeType = file.type;
    const reader = new FileReader();
    reader.onload = () => {
      const _message = {
        user,
        file: reader.result,
        conversa,
        timestamp: new Date().getTime(),
        status: 'MESSAGE',
        mimeType,
      }
      $websocket.send(JSON.stringify(_message));
    };
    reader.onerror = error => console.warn('⚠ Não foi possível enviar o arquivo.', error);
    reader.readAsDataURL(file);
  }
}

/**
 * Método para atualizar o status de "digitando" do usuário na conversa
 * @param {boolean} typing está digitando ou parou de digitar
 * @param {object} user usuário que está digitando
 * @param {object} conversa conversa que o usuário está digitando
 */
export function typing(typing, user, conversa) {
  if ($websocket instanceof WebSocket && $config.CONNECTION === 'CONNECTED') {
    const _typing = {
      user,
      conversa,
      typing,
      status: 'TYPING'
    };
    $websocket.send(JSON.stringify(_typing));
  }
}

/**
 * Método para alterar o status do usuário (online/offline/busy)
 * @param {string} status novo status do usuário
 * @param {object} user usuário em que o status foi alterado
 * @param {array} contatos lista de contatos do usuário
 */
export function changeStatus(status, user, contatos) {
  if ($websocket instanceof WebSocket && $config.CONNECTION === 'CONNECTED') {
    const _status = {
      user,
      status,
      contatos,
      status: 'STATUS'
    };
    $websocket.send(JSON.stringify(_status));
  }
}

/**
 * Método que retorna a lista de contatos
 * @returns {array} Array com os contatos
 */
export async function getContatos() {
  try {
    if (window.navigator.onLine) {
      const response = await $axios.get('/chat/contatos');
      const contatos = response.data;
      if (Array.isArray(contatos)) {
        $storage['contatos'] = contatos;
      }
      updateStorage();
      return contatos;
    } else {
      const storage = getStorage();
      return storage.contatos;
    }
  } catch (e) {
    console.log('⚠ Erro ao recuperar a lista de contatos', e)
  }
}

/**
 * Método que retorna a lista de conversas
 * @returns {Array} Array com as conversas
 */
export async function getConversas() {
  try {
    if (window.navigator.onLine) {
      const response = await $axios.get('/chat/conversas');
      const conversas = response.data;
      $storage['conversas'] = conversas;
      if (Array.isArray(conversas)) {
        $storage.conversas.forEach(conversa => {
          if (conversa.mensagens) conversa.mensagens = conversa.mensagens.slice(0, 10)
        });
      }
      updateStorage();
      return conversas;
    } else {
      const storage = getStorage();
      return storage.conversas;
    }
  } catch (e) {
    console.log('⚠ Erro ao recuperar a lista de conversas', e);
  }
}

/**
 * Método para adicionar um contato
 * @param {object} contato Contato a ser adicionado
 * @returns {array} retorna novo array de contatos
 */
export async function addContato(contato) {
  try {
    const response = await $axios.post('/chat/contatos', contato);
    return response.data;
  } catch (e) {
    console.log('⚠ Erro ao adicionar contato', e);
  }
}

/**
 * Método para criar uma conversa
 * @param {object} contato Contato a começar uma conversa
 * @returns {array} retona novo array de conversas
 */
export async function addConversa(contato) {
  try {
    const response = await $axios.post('/chat/conversas', contato);
    return response.data;
  } catch (e) {
    console.log('⚠ Erro ao adicionar conversa', e);
  }
}

/**
 * Método para remover um contato da lista
 * @param {object} contato Contato a ser excluído
 * @returns {array} retorna novo array de contatos
 */
export async function removeContato(contato) {
  try {
    const response = await $axios.delete('/chat/contatos/' + contato.id);
    return response.data;
  } catch (e) {
    console.log('⚠ Erro ao remover contato', e);
  }
}

/**
 * Método para remover uma conversa
 * @param {object} conversa Conversa a ser excluída
 * @returns {array} retona novo array de conversas
 */
export async function removeConversa(conversa) {
  try {
    const response = await $axios.delete('/chat/conversas/' + conversa.id);
    return response.data;
  } catch {
    console.log('⚠ Erro ao remover conversa', e);
  }
}

/**
 * fecha conexão com o websocket
 */
export function disconnect() {
  if ($websocket instanceof WebSocket) {
    $config.CONNECTION = 'DISCONNECTED';
    if ($config.onconnection) {
      $config.onconnection($config.CONNECTION);
    }
    $websocket.send(JSON.stringify({
      status: $config.CONNECTION,
      user: $config.USER,
    }));
    window.localStorage.clear();
    $websocket.close();
  }
}

/**
 * @returns {object} retorna o objeto de configuração
 */
export function getConfig() {
  return $config;
}
