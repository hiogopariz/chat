## API Reference

Each method can be destructed from the object returned by `require('chat-univision-lib')`

- [API Reference](#api-reference)
  - [setConfig(config)](#setconfigconfig)
  - [getConfig()](#getconfig)
  - [connect()](#connect)

---

### setConfig(config)

Set the chat configuration.
Object `config` attributes:

- `config.HEADERS` headers for HTTP requests.
- `config.HTTP_URL` url for HTTP requests.
- `config.WS_URL` url for WS connection.
- `config.USER` user logged in.
- `config.CONNECTION` similar to WebSocket.readyState, readonly.
- `config.onconnection` function that runs after [connect()](#connect).

### getConfig()

returns the `config` params:

```js
  HEADERS: string,
  HTTP_URL: string,
  WS_URL: string,
  USER: string,
  CONNECTION: 'DISCONNECTED' | 'CONNECTED',
  onconnection: function,
```
### connect()

