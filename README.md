# 🗨 WebSocket chat library

## Features

- 📜 [Rollup.js](https://rollupjs.org/guide/en) configuration providing compatibility with different module systems (CommonJS, ECMAScript, UMD for `<script>` tags)
- 🃏 [Jest](http://jestjs.io/) setup with watch mode working
- 🐈 [Yarn](https://yarnpkg.com/) with lock file, [pinned](https://renovatebot.com/docs/dependency-pinning/) devDependencies and fixed Yarn version in .yarnrc using [Yarn policies](https://yarnpkg.com/lang/en/docs/cli/policies/)
- 🏗 [.nvmrc](https://github.com/creationix/nvm) file to enforce the Node.js version for contributors and continuous integration
- ⚡️ Ready to publish and use

## [API refference](docs/api_refference.md)

## Developer environment requirements

To run this project, you will need:

- Node.js >= v10.5.0, use nvm - [install instructions](https://github.com/creationix/nvm#install-script)
- Yarn >= v1.7.0 - [install instructions ("Alternatives" tab)](https://yarnpkg.com/en/docs/install#alternatives-rc)

## Running tests

```sh
yarn
yarn test
yarn test --watch
```

## Dev mode

When developing you can run:

```
yarn watch
```

This will regenerate the build files each time a source file is changed and serve on http://127.0.0.1:5000.

### Previewing umd build in the browser

If your package works in the browser, you can open `dev/index.html` to try it out.

## Publishing

```sh
npm publish
```
